//
//  ViewController.m
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"movieTheater.png"];
    UIImageView *iv = [[UIImageView alloc] initWithImage:image];
    [self.viewOfController addSubview:iv];
    NSLog(@"IS THIS EVEN BEING USED?"); //Nope.
	 //Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
