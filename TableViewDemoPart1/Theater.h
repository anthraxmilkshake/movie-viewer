//
//  Theater.h
//  TableViewDemoPart
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Theater : NSObject

-(id) initWithDictionary: (NSDictionary *) dictionary;

- (NSString *) theaterName;
- (NSString *) line1address;
- (NSString *) cityName;
- (NSString *) zipCode;
- (NSString *) state;
- (void) print;

@end
