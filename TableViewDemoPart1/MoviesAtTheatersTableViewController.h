//
//  MoviesAtTheatersTableViewController.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theater.h"
#import "City.h"
@interface MoviesAtTheatersTableViewController : UITableViewController
-(instancetype) initWithTheater: (Theater *) theater inCity: (City*) city;
@end
