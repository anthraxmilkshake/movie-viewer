//
//  City.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject
-(id) initWithDictionary: (NSDictionary *) dictionary;
- (NSString *) cityName;

@end
