//
//  CitysDataSource.m
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "CitysDataSource.h"
#import "City.h"

@interface CitysDataSource ()

@property(nonatomic) NSMutableArray *allCities;

@end
@implementation CitysDataSource

-(instancetype) initWithJSONArray:(NSArray *)jsonArray
{
    if( (self = [super init]) == nil )
        return nil;
    
    _allCities = [[NSMutableArray alloc] init];
    for ( NSDictionary *cityTuple in jsonArray ) {
        City *city = [[City alloc] initWithDictionary:cityTuple];
        [self.allCities addObject: city];
    }
    return self;
}
-(City *) cityWithName: (NSString *) cityName
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityName = %@", cityName];
    NSArray *city = [self.allCities filteredArrayUsingPredicate:predicate];
    return [city count] == 0 ? nil : [city objectAtIndex: 0];
}
-(NSArray *) getAllCities
{
    return self.allCities;
}

-(City *) cityAtIndex: (int) idx
{
    return [self.allCities objectAtIndex:idx];
}

-(int) numberOfCities
{
    return [self.allCities count];
}
@end
