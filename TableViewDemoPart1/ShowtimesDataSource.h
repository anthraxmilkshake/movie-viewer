//
//  ShowTimeDataSource.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowtimesDataSource : NSObject

@property (nonatomic) BOOL dataReadyForUse;

-(instancetype) initWithJSONArray:(NSArray *)jsonArray;
-(NSArray *) showtimeForMovie: (NSString *) movieName atTheater: (NSString *) theaterName;
-(void) print;

@end
