//
//  MovieAtTheater.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieAtTheater : NSObject

-(instancetype) initWithDictionary: (NSDictionary *) dictionary;

- (NSString *) movieName;
- (NSString *) theaterName;
- (NSString *) cityName;
- (void) print;

@end
