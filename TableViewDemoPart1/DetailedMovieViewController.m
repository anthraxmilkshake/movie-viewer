//
//  DetailedMovieViewController.m
//  CS470Feb27
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "DetailedMovieViewController.h"
#import "Movie.h"
#import "Theater.h"
#import "ShowtimesDataSource.h"
#import "Showtime.h"
#import "SchemaDataSource.h"

@interface DetailedMovieViewController ()
@property(nonatomic) Movie *movie;
@property (nonatomic) Theater *theater;
@property (nonatomic) Boolean hasShowtimes;
@property (nonatomic) ShowtimesDataSource * stDataSource;

@end

@implementation DetailedMovieViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(instancetype) initWithMovie: (Movie *) movie
{
    if( (self = [super init]) == nil )
        return nil;
    
    self.movie = movie;
    self.hasShowtimes = NO;
    return self;
}
-(instancetype) initWithMovie:(Movie *)movie andTheater: (Theater*) theater
{
    if( (self = [super init]) == nil )
        return nil;
    
    self.stDataSource = [[SchemaDataSource sharedInstance] showtimesDataSource];
    self.movie = movie;
    self.theater = theater;
    self.hasShowtimes = YES;
    return self;
    
}


- (void)viewDidLoad
{
    /* 
     This is a quick (and to some extent hard-coded) demonstration of the use of a view-controller
     that gets pushed on the navigation stack as a result of having tapped a table-view cell.
     */
    
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    int HeightAdjust = 0;
    if (self.hasShowtimes) {
        HeightAdjust = 90;
    }
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    NSURL *url = [NSURL URLWithString: [self.movie imageNameForDetailedView]];
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:imageData];
    CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
    CGRect frame = CGRectMake(50+(HeightAdjust/4), 10,  appFrame.size.width - (100+(HeightAdjust/2)), 200-HeightAdjust);
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = frame;
    [self.view addSubview:imageView];
    CGRect webFrame = CGRectMake(10, 220-(HeightAdjust), appFrame.size.width - 20, 250-HeightAdjust);

    UITextView * descWebView = [[UITextView alloc]initWithFrame:webFrame];
    [descWebView setText:[self.movie htmlDescriptionForDetailedView]];
    [descWebView setTextColor:[UIColor greenColor]];
    [descWebView setBackgroundColor:[UIColor blackColor]];
    [descWebView setFont:[UIFont systemFontOfSize:15]];
    [descWebView setEditable:NO];
    
    
    
    if (self.hasShowtimes) {
        CGRect frame = CGRectMake(10, 300, appFrame.size.width-20, 100);
        UITextView * timeShower;
        timeShower = [[UITextView alloc] initWithFrame:frame];
        timeShower.hidden = NO;
        [timeShower setBackgroundColor:[UIColor blackColor]];
        [timeShower setTextColor:[UIColor greenColor]];
        
        NSMutableString * movieTimeString = [NSMutableString stringWithString: @"Time's this movie is being shown (scroll for more):"];
        for (int i = 0; i<[[self.stDataSource showtimeForMovie:[self.movie title] atTheater:[self.theater theaterName]] count]; i++) {
            [movieTimeString appendString:@"\n"];
            [movieTimeString appendString:[[[self.stDataSource showtimeForMovie:[self.movie title] atTheater:[self.theater theaterName]]objectAtIndex:i]timeString]];
        }
        timeShower.text =  (NSString*)movieTimeString;
        timeShower.textAlignment = NSTextAlignmentCenter;
        timeShower.userInteractionEnabled = YES;
        timeShower.editable = NO;
        timeShower.font = [UIFont fontWithName:@"Helvetica" size:20];

        [self.view addSubview:timeShower];

        
        
    }
    [self.view addSubview:descWebView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
