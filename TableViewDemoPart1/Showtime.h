//
//  ShowTime.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Showtime : NSObject

-(id) initWithDictionary: (NSDictionary *) dictionary;

- (NSString *) timeString;
- (NSString *) movieTitle;
- (NSString *) theaterName;
- (void) print;

@end
