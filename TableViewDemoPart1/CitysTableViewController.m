//
//  CitysTableViewController.m
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "CitysTableViewController.h"
#import "CitysDataSource.h"
#import "SchemaDataSource.h"
#import "MoviesAtTheatersTableViewController.h"
#import "Theater.h"
#import "MoviesTableViewController.h"

@interface CitysTableViewController ()
@property(nonatomic) SchemaDataSource *dataSource;
@property(nonatomic) TheatersDataSource *theatersDataSource;
@property(nonatomic) CitysDataSource *citysDataSource;
@property(nonatomic) UIActivityIndicatorView *activityIndicator;

@end
static NSString *CellIdentifier = @"Cell";
@implementation CitysTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        UIImage *tabImage = [UIImage imageNamed:@"city.png"];
        self.tabBarItem.image = tabImage;
        [self.tableView setBackgroundColor:[UIColor greenColor]];
        
        
        
        self.title = @"City";
        [[SchemaDataSource sharedInstance] addObserver: self
                                            forKeyPath: @"dataSourceReady"
                                               options: NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                                               context:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    self.dataSource = [SchemaDataSource sharedInstance];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.activityIndicator setCenter: self.view.center];
    [self.view addSubview: self.activityIndicator];
}
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.citysDataSource = [[SchemaDataSource sharedInstance] citysDataSource];
    self.theatersDataSource = [[SchemaDataSource sharedInstance] theatersDataSource];
    [self.tableView reloadData];
    [self.activityIndicator stopAnimating];

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) refreshTableView: (UIRefreshControl *) rControl
{
    if( [self.dataSource dataSourceReady] )
        [self.tableView reloadData];
    [rControl endRefreshing];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return [self.citysDataSource numberOfCities];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    // Return the number of rows in the section.
    City *city = [self.citysDataSource cityAtIndex: section];
    NSArray *theaters = [self.theatersDataSource moviesInCity:city.cityName];
    return [theaters count];
}
- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    City *city = [self.citysDataSource cityAtIndex: section];
    NSString *text = [NSString stringWithFormat:@"%@", city.cityName];
    return text;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    City *city = [self.citysDataSource cityAtIndex: [indexPath section]];
    NSArray *theatersInCity = [self.theatersDataSource moviesInCity:city.cityName];

    cell.textLabel.text = [ (Theater *) [theatersInCity objectAtIndex:[indexPath row]] theaterName];
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor greenColor];
    
    return cell;
}
- (NSInteger) tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 2;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if( section == 0 )
        return 35;
    return 15;
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    Theater *theater = [self.theatersDataSource theaterAtIndex:[indexPath row]];
    City *city = [self.citysDataSource cityWithName:[theater cityName]];
    MoviesAtTheatersTableViewController *mc = [[MoviesAtTheatersTableViewController alloc] initWithTheater:theater inCity:city];
  
    
    [self.navigationController pushViewController:mc animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
