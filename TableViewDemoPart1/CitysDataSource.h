//
//  CitysDataSource.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadAssistant.h"
#import "City.h"

@interface CitysDataSource : NSObject

@property (nonatomic) BOOL dataReadyForUse;

-(instancetype) initWithJSONArray:(NSArray *) jsonArray;
-(City *) cityWithName: (NSString *) cityName;
-(NSMutableArray *) getAllCities;
-(City *) cityAtIndex: (int) idx;
-(int) numberOfCities;
@end
