//
//  TheaterDataSource.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadAssistant.h"
#import "Theater.h"

@interface TheatersDataSource : NSObject

@property (nonatomic) BOOL dataReadyForUse;

-(instancetype) initWithJSONArray:(NSArray *) jsonArray;
-(Theater *) theaterWithName: (NSString *) theaterName;
-(NSMutableArray *) getAllTheaters;
-(Theater *) theaterAtIndex: (int) idx;
-(int) numberOfTheaters;
-(NSString *) theaterTabBarTitle;
-(NSString *) theaterTabBarImage;
-(NSString *) theaterBarButtonItemBackButtonTitle;
-(BOOL) deleteTheaterAtIndex: (NSInteger) idx;
-(void) print;
-(NSArray *) moviesInCity:(NSString *) cityName;

@end
