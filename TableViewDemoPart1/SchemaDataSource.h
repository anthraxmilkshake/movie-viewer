//
//  SchemaDataSource.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MoviesDataSource.h"
#import "TheatersDataSource.h"
#import "ShowtimesDataSource.h"
#import "DownloadAssistant.h"
#import "MoviesAtTheatersDataSource.h"
#import "CitysDataSource.h"

@protocol DataSourceReadyForUseDelegate;

@interface SchemaDataSource : NSObject<WebDataReadyDelegate>

@property (nonatomic) id<DataSourceReadyForUseDelegate> delegate;

@property(nonatomic) MoviesDataSource * moviesDataSource;
@property(nonatomic) TheatersDataSource * theatersDataSource;
@property(nonatomic) ShowtimesDataSource *showtimesDataSource;
@property(nonatomic) CitysDataSource *citysDataSource;
@property(nonatomic) MoviesAtTheatersDataSrouce *moviesAtTheatersDataSource;

@property(nonatomic) BOOL dataSourceReady;

+(SchemaDataSource *) sharedInstance;

@end

@protocol DataSourceReadyForUseDelegate <NSObject>

@optional

-(void) dataSourceReadyForUse: (SchemaDataSource *) dataSource;

@end
