//
//  MovieAtTheaterDataSrouce.h
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.//

#import <Foundation/Foundation.h>

@interface MoviesAtTheatersDataSrouce : NSObject

-(instancetype) initWithJSONArray:(NSArray *)jsonArray;
-(NSArray *) moviesForTheater: (NSString *) theaterName inCity: (NSString *) cityName;

@end
