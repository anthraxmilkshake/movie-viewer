//
//  MoviesAtTheatersTableViewController.m
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "MoviesAtTheatersTableViewController.h"
#import "SchemaDataSource.h"
#import "DetailedMovieViewController.h"
#import "MoviesAtTheatersDataSource.h"
#import "MovieAtTheater.h"

@interface MoviesAtTheatersTableViewController ()

@property(nonatomic) SchemaDataSource *dataSource;
@property(nonatomic) MoviesDataSource *moviesDataSource;
@property(nonatomic) MoviesAtTheatersDataSrouce *moviesAtTheatersDataSource;
@property(nonatomic) UIActivityIndicatorView *activityIndicator;
@property(nonatomic) Theater *theater;
@property(nonatomic) City *city;


@end
static NSString *CellIdentifier = @"Cell";

@implementation MoviesAtTheatersTableViewController

-(instancetype) initWithTheater:(Theater *)theater inCity: (City *) city
{
    if( (self = [super init]) == nil )
        return nil;
    
    self.theater = theater;
    self.city = city;
    [self.tableView reloadData];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.moviesAtTheatersDataSource = [[SchemaDataSource sharedInstance] moviesAtTheatersDataSource];
        self.moviesDataSource = [[SchemaDataSource sharedInstance] moviesDataSource];
        [self.tableView reloadData];
        self.title = @"Movies At Theater";
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    
    self.dataSource = [SchemaDataSource sharedInstance];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.activityIndicator setCenter: self.view.center];
    [self.view addSubview: self.activityIndicator];
    [self.tableView setBackgroundColor:[UIColor greenColor]];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if( ! [self.dataSource dataSourceReady] ) {
        [self.activityIndicator startAnimating];
        [self.activityIndicator setHidesWhenStopped: YES];
    }
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
   
   
    return [[self.moviesAtTheatersDataSource moviesForTheater:[self.theater theaterName] inCity:[self.city cityName]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    MovieAtTheater *mat = [[self.moviesAtTheatersDataSource moviesForTheater:[self.theater theaterName] inCity:[self.city cityName]] objectAtIndex:[indexPath row]];

    // Configure the cell...
    cell.textLabel.text = [mat movieName];
  
    
    //The Following commented code does something pretty cool
    //but it downloads so often that it's too slow to use as implemented.
    
    /*
     NSURL *url = [NSURL URLWithString: [movie imageNameForDetailedView]];
     NSData *imageData = [NSData dataWithContentsOfURL:url];
     UIImage *image = [UIImage imageWithData:imageData];
     cell.imageView.image = image;
     */
    
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor greenColor];
    return cell;
}

-(void) refreshTableView: (UIRefreshControl *) sender
{
    [self.tableView reloadData];
    [sender endRefreshing];
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.moviesDataSource deleteMovieAtIndex:[indexPath row]];
    [self.tableView deleteRowsAtIndexPaths: @[indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (NSIndexPath *) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // deselect a selected row when it is tapped for the second time.
    if( [self.tableView cellForRowAtIndexPath:indexPath].selected ) {
        [self.tableView deselectRowAtIndexPath: indexPath animated:YES];
        return nil;
    }
    return indexPath;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Did select row %d", [indexPath row] );
   
   
    MovieAtTheater *mat = [[self.moviesAtTheatersDataSource moviesForTheater:[self.theater theaterName] inCity:[self.city cityName]] objectAtIndex:[indexPath row]];
    Movie *movie = [self.moviesDataSource movieWithTitle:[mat movieName]];

    DetailedMovieViewController *dc = [[DetailedMovieViewController alloc] initWithMovie:movie andTheater:self.theater];
    [self.navigationController pushViewController:dc animated:YES];
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
