//
//  ShowTimeDataSource.m
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "ShowtimesDataSource.h"
#import "Showtime.h"

BOOL _debug = NO;

@interface ShowtimesDataSource ()

@property (nonatomic) NSMutableArray *allShowtimes;


@end

@implementation ShowtimesDataSource


-(instancetype) initWithJSONArray:(NSArray *)jsonArray
{
    if( (self = [super init]) == nil )
        return nil;
    _allShowtimes = [[NSMutableArray alloc]init];
    for ( NSDictionary *showtimeTuple in jsonArray ) {
        Showtime *showtime = [[Showtime alloc] initWithDictionary: showtimeTuple];
        if( _debug) [showtime print];
        [self.allShowtimes addObject: showtime];
        [showtime print];
    }
   
    return self;
}

-(NSArray *) showtimeForMovie: (NSString *) movieName atTheater: (NSString *) theaterName;
{
   
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"movieTitle = %@ and theaterName = %@", movieName, theaterName];

    return [self.allShowtimes filteredArrayUsingPredicate:predicate];
}

-(void) print
{
    NSLog(@"Printing showtimes...");
    for( Showtime *showtime in self.allShowtimes )
        [showtime print];
    NSLog(@"Printing showtimes ends.");
}


@end
