//
//  City.m
//  TableViewDemoPart1
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "City.h"
@interface City ()

@property(nonatomic) NSMutableDictionary *cityAttrs;

@end

@implementation City
-(id) initWithDictionary: (NSDictionary *) dictionary
{
	if( (self = [super init]) == nil )
		return nil;
	self.cityAttrs = [NSMutableDictionary dictionaryWithDictionary: dictionary];
	return self;
}
- (NSString *) cityName
{
    return [self.cityAttrs objectForKey:@"cityName"];
}



@end
