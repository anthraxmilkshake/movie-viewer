//
//  DetailedMovieViewController.h
//  CS470Feb27
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import "Theater.h"

@interface DetailedMovieViewController : UIViewController

-(instancetype) initWithMovie: (Movie *) movie;
-(instancetype) initWithMovie:(Movie *) movie andTheater: (Theater*) theater;


@end
