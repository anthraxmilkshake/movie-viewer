//
//  DownloadAssistant.h
//  SharedServicesActivityReportingSystem
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebDataReadyDelegate;

@interface DownloadAssistant : NSObject<NSURLConnectionDelegate>

@property (nonatomic) id<WebDataReadyDelegate> delegate;

-(void) downloadContentsOfURL: (NSURL *) url;
-(NSData *) downloadedData;

@end

@protocol WebDataReadyDelegate<NSObject>

@required

-(void) acceptWebData: (NSData *) webData forURL: (NSURL *) url;

@end
